#include <assert.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <cmath>

#include <GL/glew.h>
#include <GL/glut.h>

#include <Cg/cg.h>
#include <Cg/cgGL.h>

CGcontext context;
CGprofile vertexProfile, fragmentProfile;

int w_width=512, w_height=512;
float near, top, bottom;
float epsilon = 1e0;
float screenSpaceRadius = 3;

struct Surfel
{
    GLfloat pos[3];
    GLfloat color[3]; // Changed from 4 to save space
    GLfloat uvec[3];
    GLfloat vvec[3];
};

int numpoints;
Surfel *pts;
GLuint ptsVbo;

static struct
{
    CGprogram   program;
    CGparameter ModelViewProj;
    CGparameter ModelView;
    CGparameter ModelViewIT;
    CGparameter wsize;
    CGparameter near;
    CGparameter top;
    CGparameter bottom;
    CGparameter screenSpaceRadius;
} vshader;

static struct
{
    CGprogram   program;
    CGparameter unproj_scale;
    CGparameter unproj_offset;
    CGparameter near;
    CGparameter zb_scale;
    CGparameter zb_offset;
    CGparameter epsilon;
    CGparameter screenSpaceRadius;
} fshader;

static struct  {
    GLuint fbo;
    GLuint depthbuffer;
    GLuint colorbuffer;
    GLuint normalbuffer;
} splatbuffer = {0,0,0,0};

static struct {
    CGprogram   program;
    CGparameter colors;
    CGparameter normals;
    CGparameter unproj_scale;
    CGparameter unproj_offset;
    CGparameter near;
} deferredshader;

const GLenum buffers[] = {GL_COLOR_ATTACHMENT0_EXT, GL_COLOR_ATTACHMENT1_EXT};

void read_points(const char *fname)
{
    FILE *f = fopen(fname, "rb");
    assert(f);
    fread(&numpoints, sizeof(int), 1, f);
    printf("Reading %i points from %s ...\n", numpoints, fname);
    pts = new Surfel[numpoints];
    fread(pts, sizeof(Surfel)*numpoints, 1, f);    
    fclose(f);
    
    //fill vbo
    glGenBuffers(1, &ptsVbo);
    glBindBuffer(GL_ARRAY_BUFFER, ptsVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Surfel)*numpoints, pts, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key) {
        case 'q':
        case 'Q':
        case 27:
            exit(0);
            break;
    }
}

void handleCgError() 
{   
    CGerror error = cgGetError();
    if (error != CG_NO_ERROR)
        fprintf(stderr, "Cg error: %s\n", cgGetErrorString(error));
}

void display() 
{
    int curTime = glutGet(GLUT_ELAPSED_TIME);

    glClearColor(0.0f, 0.0f, 0.0f, 1e-8);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    GLdouble fW, fH;
    float aspect = (float)w_width/(float)w_height;
    near = 0.1;
    float far = 100.0;
    float fovY = 30.0;
    fH = tan( fovY / 360 * M_PI ) * near;
    fW = fH * aspect;
    top = fH;
    bottom = -fH;
    glFrustum( -fW, fW, -fH, fH, near, far);

    cgGLEnableProfile(vertexProfile);
    cgGLBindProgram(vshader.program);
    cgGLEnableProfile(fragmentProfile);
    cgGLBindProgram(fshader.program);

    //set vertex shader uniforms
    cgGLSetStateMatrixParameter(
        vshader.ModelViewProj,
        CG_GL_MODELVIEW_PROJECTION_MATRIX,
        CG_GL_MATRIX_IDENTITY);
    cgGLSetStateMatrixParameter(
        vshader.ModelView,
        CG_GL_MODELVIEW_MATRIX,
        CG_GL_MATRIX_IDENTITY);
    cgGLSetStateMatrixParameter(
        vshader.ModelViewIT,
        CG_GL_MODELVIEW_MATRIX,
        CG_GL_MATRIX_INVERSE_TRANSPOSE);

    cgGLSetParameter2f( vshader.wsize, w_width, w_height);
    cgGLSetParameter1f(  vshader.near, near);
    cgGLSetParameter1f(   vshader.top, top);
    cgGLSetParameter1f(vshader.bottom, bottom);
    cgGLSetParameter1f(vshader.screenSpaceRadius, screenSpaceRadius);

    //set fragment shader uniforms
    cgGLSetParameter2f(fshader.unproj_scale, (2*fW)/w_width, (2*fH)/w_height);
    cgGLSetParameter2f(fshader.unproj_offset, fW, fH);
    cgGLSetParameter1f(fshader.near, near);
    cgGLSetParameter1f(fshader.zb_scale, far*near/(far-near));
    cgGLSetParameter1f(fshader.zb_offset, far/(far-near));
    cgGLSetParameter1f(fshader.epsilon, epsilon);
    cgGLSetParameter1f(fshader.screenSpaceRadius, screenSpaceRadius);
    
    //set deferred shader uniforms
    cgGLSetParameter2f(deferredshader.unproj_scale, (2*fW)/w_width, (2*fH)/w_height);
    cgGLSetParameter2f(deferredshader.unproj_offset, fW, fH);
    cgGLSetParameter1f(deferredshader.near, near);

    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(4, 0, -3, 0, 0, 0, 0, 1, 0);
    
    glRotatef((float)curTime/20.0, 0, 1, 0);
    
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
    
    //glBindBuffer(GL_ARRAY_BUFFER, 0);
    //char *base = (char *) pts;
    glBindBuffer(GL_ARRAY_BUFFER, ptsVbo);
    char *base = 0;
    
    glClientActiveTexture(GL_TEXTURE0);
    glEnableClientState (GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(3, GL_FLOAT, sizeof(Surfel), base + offsetof(Surfel, color));

    glClientActiveTexture(GL_TEXTURE1);
    glEnableClientState (GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(3, GL_FLOAT, sizeof(Surfel), base + offsetof(Surfel, uvec));

    glClientActiveTexture(GL_TEXTURE2);
    glEnableClientState (GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(3, GL_FLOAT, sizeof(Surfel), base + offsetof(Surfel, vvec));

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, sizeof(Surfel), base + offsetof(Surfel, pos));

    //switch fbo
    glBindFramebuffer(GL_FRAMEBUFFER, splatbuffer.fbo);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawBuffers(2, buffers);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //depth pass
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    glDepthMask(GL_TRUE);
    glDrawArrays(GL_POINTS, 0, numpoints);
    //color pass
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    //glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE, GL_ONE, GL_ONE);
    cgGLSetParameter1f(fshader.epsilon, 0);
    glDrawArrays(GL_POINTS, 0, numpoints);
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glClientActiveTexture(GL_TEXTURE0);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);

    cgGLUnbindProgram(vertexProfile);
    cgGLDisableProfile(vertexProfile);
    cgGLUnbindProgram(fragmentProfile);
    cgGLDisableProfile(fragmentProfile);


    cgGLEnableProfile(fragmentProfile);
    cgGLBindProgram(deferredshader.program);


    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture (GL_TEXTURE_2D, splatbuffer.colorbuffer);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture (GL_TEXTURE_2D, splatbuffer.normalbuffer);
    glEnable(GL_TEXTURE_2D);
    glBegin( GL_QUADS );
        glTexCoord2f(0,0);
        glVertex2f(-1.0f, -1.0f);
        glTexCoord2f(1,0);
        glVertex2f(1.0f, -1.0f);
        glTexCoord2f(1,1);
        glVertex2f(1.0f, 1.0f);
        glTexCoord2f(0,1);
        glVertex2f(-1.0f, 1.0f);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    cgGLUnbindProgram(fragmentProfile);
    cgGLDisableProfile(fragmentProfile);


    //print any opengl errors
    int error;
    while ((error = glGetError()) != GL_NO_ERROR) {
        printf("GLerror: %s\n",  gluErrorString(error));
    }

    //print cg errors:
    handleCgError();

    glutSwapBuffers();
}

/* Choose profiles, set optimal options */
void chooseCgProfiles()
{
    vertexProfile = cgGLGetLatestProfile(CG_GL_VERTEX);
    cgGLSetOptimalOptions(vertexProfile);
    fragmentProfile = cgGLGetLatestProfile(CG_GL_FRAGMENT);
    cgGLSetOptimalOptions(fragmentProfile);
    printf("vertex profile:   %s\n",
         cgGetProfileString(vertexProfile));
    printf("fragment profile: %s\n",
         cgGetProfileString(fragmentProfile));

}

/* Load Cg program from disk */
CGprogram loadCgProgram(CGprofile profile, const char *filename)
{
    CGprogram program;
    assert(cgIsContext(context));

    fprintf(stderr, "Cg program %s creating.\n", filename);
    program = cgCreateProgramFromFile(context, CG_SOURCE,
            filename, profile, NULL, NULL);
    
    if(!cgIsProgramCompiled(program)) {
        printf("%s\n",cgGetLastListing(context));
        exit(1);
    }
    
    fprintf(stderr, "Cg program %s loading.\n", filename);
    cgGLLoadProgram(program);
    
    return program;
}

CGparameter getCGParameter( CGprogram program, const char * name)
{
    CGparameter parameter = cgGetNamedParameter(program, name);
    if (!parameter)
        fprintf(stderr,"Parameter %s was not defined in the shader\n", name);
    return parameter;
}

void loadCgPrograms()
{
    /* Load all Cg programs that are used with loadCgProgram */
    //loadCgProgram(vertexProfile, "vertexShader.cg");
    //loadCgProgram(fragmentProfile, "fragmentShader.cg");

    vshader.program = loadCgProgram(vertexProfile, "splat_v.cg");
    vshader.ModelViewProj     = getCGParameter(vshader.program, "ModelViewProj");
    vshader.ModelView         = getCGParameter(vshader.program, "ModelView");
    vshader.ModelViewIT       = getCGParameter(vshader.program, "ModelViewIT");
    vshader.wsize             = getCGParameter(vshader.program, "wsize");
    vshader.near              = getCGParameter(vshader.program, "near");
    vshader.top               = getCGParameter(vshader.program, "top");
    vshader.bottom            = getCGParameter(vshader.program, "bottom");
    vshader.screenSpaceRadius = getCGParameter(vshader.program, "screenSpaceRadius");

    fshader.program = loadCgProgram(fragmentProfile, "splat_f.cg");
    fshader.unproj_scale   = getCGParameter(fshader.program, "unproj_scale");
    fshader.unproj_offset  = getCGParameter(fshader.program, "unproj_offset");
    fshader.near           = getCGParameter(fshader.program, "near");
    fshader.zb_scale       = getCGParameter(fshader.program, "zb_scale");
    fshader.zb_offset      = getCGParameter(fshader.program, "zb_offset");
    fshader.epsilon        = getCGParameter(fshader.program, "epsilon");
    fshader.screenSpaceRadius = getCGParameter(fshader.program, "screenSpaceRadius");

    deferredshader.program = loadCgProgram(fragmentProfile, "deferred_f.cg");
    deferredshader.colors         = getCGParameter(deferredshader.program, "colors");
    deferredshader.normals        = getCGParameter(deferredshader.program, "normals");
    deferredshader.unproj_scale   = getCGParameter(deferredshader.program, "unproj_scale");
    deferredshader.unproj_offset  = getCGParameter(deferredshader.program, "unproj_offset");
    deferredshader.near           = getCGParameter(deferredshader.program, "near");
}

void idle()
{
    glutPostRedisplay();
}

void reshape(int width, int height)
{
    w_width = width;
    w_height = height;
    glViewport(0, 0, w_width, w_height);

    //fbo magic
    //delete the old buffers
    glDeleteFramebuffers(1, &(splatbuffer.fbo));
    glDeleteTextures(1, &(splatbuffer.depthbuffer));
    glDeleteTextures(1, &(splatbuffer.colorbuffer));
    glDeleteTextures(1, &(splatbuffer.normalbuffer));
    //init the new color buffer
    glGenTextures(1, &(splatbuffer.colorbuffer));
    glBindTexture(GL_TEXTURE_2D, splatbuffer.colorbuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D, //target
        0,             //mipmap level
        GL_RGBA16F,    //internalFormat
        w_width,       //width
        w_height,      //height
        0,             //border
        GL_RGBA,       //format
        GL_FLOAT,      //type
        0);            //data
    glBindTexture(GL_TEXTURE_2D, 0);
    //normal buffer
    glGenTextures(1, &(splatbuffer.normalbuffer));
    glBindTexture(GL_TEXTURE_2D, splatbuffer.normalbuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D, //target
        0,             //mipmap level
        GL_RGBA16F,    //internalFormat
        w_width,       //width
        w_height,      //height
        0,             //border
        GL_RGBA,       //format
        GL_FLOAT,      //type
        0);            //data
    glBindTexture(GL_TEXTURE_2D, 0);
    //depth buffer
    glGenTextures(1, &(splatbuffer.depthbuffer));
    glBindTexture(GL_TEXTURE_2D, splatbuffer.depthbuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D, //target
        0,             //mipmap level
        GL_DEPTH_COMPONENT32,//internalFormat
        w_width,       //width
        w_height,      //height
        0,             //border
        GL_DEPTH_COMPONENT,//format
        GL_FLOAT,      //type
        0);            //data
    glBindTexture(GL_TEXTURE_2D, 0);
    //frame buffer
    glGenFramebuffers(1, &(splatbuffer.fbo));
    glBindFramebuffer(GL_FRAMEBUFFER, splatbuffer.fbo);
    //attach color
    glFramebufferTexture2D(
          GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
          GL_COLOR_ATTACHMENT0,  // 2. attachment point
          GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
          splatbuffer.colorbuffer,// 4. tex ID
          0);                    // 5. mipmap level: 0(base)
    //attach normals
    glFramebufferTexture2D(
          GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
          GL_COLOR_ATTACHMENT1,  // 2. attachment point
          GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
          splatbuffer.normalbuffer,// 4. tex ID
          0);                    // 5. mipmap level: 0(base)
    //attach depth
    glFramebufferTexture2D(
          GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
          GL_DEPTH_ATTACHMENT,  // 2. attachment point
          GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
          splatbuffer.depthbuffer,// 4. tex ID
          0);                    // 5. mipmap level: 0(base)

    int error;
    if ((error = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE)
        std::printf("paniek: framebuffer fail:  %s\n",  gluErrorString(error));

    // set the old fb back
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void initGlut(int argc, char *argv[]){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(w_width, w_height);
    glutCreateWindow("Point based rendering in Cg");
    
    glewInit();
}

int main(int argc, char *argv[])
{
    initGlut(argc, argv);
    
    glutKeyboardFunc(keyboard);
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutReshapeFunc(reshape);
    
    cgSetErrorCallback(handleCgError);
    context = cgCreateContext();
    chooseCgProfiles();
    loadCgPrograms();
    
    assert(argc>=2 && argv[1]);
    read_points(argv[1]);
    
    glutMainLoop();
    
    return 0;
}


