void main(
  float4 pvec      : POSITION,
  float4 color     : TEXCOORD0,
  float3 uvec      : TEXCOORD1,
  float3 vvec      : TEXCOORD2,
  uniform float4x4 ModelViewProj,
  uniform float4x4 ModelView,
  uniform float4x4 ModelViewIT,
  uniform float2   wsize,
  uniform float    near,
  uniform float    top,
  uniform float    bottom,
  uniform float    screenSpaceRadius,

  out float4  pout  : POSITION,
  out float   psize : PSIZE,
  out float4  cout  : TEXCOORD0,
  out float3  v1    : TEXCOORD1,
  out float3  v2    : TEXCOORD2,
  out float3  v3    : TEXCOORD3,
  out float2  depthout: TEXCOORD4,
  out float2  screenSpacePosition: TEXCOORD5
  )
{
  pout  = mul(ModelViewProj, pvec);
  float4 peye = mul(ModelView, pvec);
  float4 ueye = mul(ModelView, float4(uvec,0));
  float4 veye = mul(ModelView, float4(vvec,0));
  
  float radius = sqrt(max(dot(uvec,uvec),dot(vvec,vvec)));
  
  psize = max(2.0 * screenSpaceRadius, 2.0 * radius * (-near / peye.z) * (wsize.y / (top - bottom)));

  cout = color;

  v1       = cross(peye.xyz, - veye.xyz);
  v2       = cross(-ueye.xyz, peye.xyz);
  v3       = cross(ueye.xyz, veye.xyz);
  depthout.x = dot(peye.xyz, v3);
  depthout.y = radius;//for use with the epsilon offset

  float screenX = (0.5 + 0.5 * (pout.x / pout.w)) * wsize.x; // screenX \in [0, wsize.x]
  float screenY = (0.5 + 0.5 * (pout.y / pout.w)) * wsize.y; // screenY \in [0, wsize.y]
  screenSpacePosition = float2(screenX, screenY);
  //backface culling by forcing clipping
  if (depthout.x > 0.0)
    pout.w = -1.0;
}
