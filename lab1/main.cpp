#include <cstdio>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <Cg/cg.h>
#include <Cg/cgGL.h>


int w_width=512, w_height=512;

//yay for globals!
CGcontext context = NULL;
CGprogram myVertexProgram = NULL;
CGprofile vertexProfile = CG_PROFILE_VP40;
CGprofile fragmentProfile = CG_PROFILE_FP40;
CGprogram myFragmentProgram = NULL;

CGparameter modelViewProj = NULL;
CGparameter modelView = NULL;
CGparameter modelViewIT = NULL;

CGparameter cgShininess = NULL;
float shininess = 100.0;




static void display()
{
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    cgGLSetStateMatrixParameter(
        modelViewProj,
        CG_GL_MODELVIEW_PROJECTION_MATRIX,
        CG_GL_MATRIX_IDENTITY);
    cgGLSetStateMatrixParameter(
        modelView,
        CG_GL_MODELVIEW_MATRIX,
        CG_GL_MATRIX_IDENTITY);
    cgGLSetStateMatrixParameter(
        modelViewIT,
        CG_GL_MODELVIEW_MATRIX,
        CG_GL_MATRIX_INVERSE_TRANSPOSE);
    cgGLSetParameter1f(
        cgShininess,
        shininess);

    
    cgGLEnableProfile(vertexProfile);
    cgGLBindProgram(myVertexProgram);
    cgGLEnableProfile(fragmentProfile);
    cgGLBindProgram(myFragmentProgram);


    //glutWireSphere(1.0, 10, 10);
    glutSolidSphere(3.0, 32, 32);

    cgGLDisableProfile(vertexProfile);
    cgGLDisableProfile(fragmentProfile);


    glutSwapBuffers();

    //errors tonen
    int error;
    while ((error = glGetError()) != GL_NO_ERROR) {
        printf("GLerror: %s\n",  gluErrorString(error));
    }
}

void reshape(GLint width, GLint height)
{
    w_width = width;
    w_height = height;

    glViewport(0, 0, w_width, w_height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);

    glMatrixMode (GL_PROJECTION);
    glLoadIdentity();
    gluPerspective (
        90.0, // fov
        (float)w_width / w_height,  // aspect ratio
        .05,  // near
        40);  // far

    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(
        0, 0, 5, // eye
        0, 0, 0, // lookat point
        0, 1, 0);// up

    glutPostRedisplay();
}

static void initializeGlut(int *argc, char *argv[])
{
    glutInit (argc, argv);

    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(w_width, w_height);
    glutCreateWindow(argv[0]);

    glewInit();

    reshape(w_width, w_height);

    glEnable(GL_DEPTH_TEST);

}


int initCG(){
    //vertex shader
    context = cgCreateContext();
    myVertexProgram = cgCreateProgramFromFile(
        context,
        CG_SOURCE,
        "colorful_v.cg",
        vertexProfile,
        "main", // name of entry point in our shader
        NULL);
    if(!myVertexProgram)
    {
        printf("Couldn’t load vertex program.\n");
        printf("%s\n",cgGetLastListing(context));
    }
    cgGLLoadProgram(myVertexProgram);

    //fragment shader
    myFragmentProgram = cgCreateProgramFromFile(
        context,
        CG_SOURCE,
        "colorful_f.cg",
        fragmentProfile,
        "main", // name of entry point in our shader
        NULL);
    if(!myFragmentProgram)
    {
        printf("Couldn’t load fragment program.\n");
        printf("%s\n",cgGetLastListing(context));
    }
    cgGLLoadProgram(myFragmentProgram);



    //uniforms
    modelViewProj = cgGetNamedParameter(
        myVertexProgram,
        "modelViewProj");
    if (!modelViewProj)
        printf("Parameter modelViewProj was not defined in the shader\n");
    modelView = cgGetNamedParameter(
        myVertexProgram,
        "modelView");
    if (!modelView)
        printf("Parameter modelView was not defined in the shader\n");
    modelViewIT = cgGetNamedParameter(
        myVertexProgram,
        "modelViewIT");
    if (!modelViewIT)
        printf("Parameter modelViewIT was not defined in the shader\n");

    cgShininess = cgGetNamedParameter(
        myFragmentProgram,
        "shininess");
    if (!cgShininess)
        printf("Parameter shininess was not defined in the shader\n");

    return 0;
}

void Keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27:             // ESCAPE key
        exit (0);
        break;
    case 'S':
        shininess += 5;
        break;
    case 's':
        shininess -= 5;
        break;
    }
    glutPostRedisplay();
}




int main (int argc, char *argv[])
{
    initializeGlut(&argc, argv);
    if (initCG())
        return EXIT_FAILURE;
    glutDisplayFunc (display);
    glutKeyboardFunc (Keyboard);
    glutReshapeFunc (reshape);
    glutMainLoop();
        
    // never gets here.
    return EXIT_SUCCESS;
}
