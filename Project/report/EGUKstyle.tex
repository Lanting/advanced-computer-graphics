%% BEGIN INCLUDE
\documentclass[a4paper, 10pt, twocolumn]{eguk2000}
\usepackage[square]{natbib}
\pagestyle{empty}
\begin{document}
%% END INCLUDE

\title{Single-Pass Point Rendering}
\author{{\sffamily Jeroen Lanting and Jelle Nauta}\\
\\
Advanced Computer Graphics course\\
Rijksuniversiteit Groningen\\
Groningen\\
{\tt jc.nauta@gmail.com, j@lntng.nl}\\
\\
\parbox{140truemm}{\normalsize
{\bfseries Abstract}\\
% This should contain a brief description
% of the paper, setting it in the context of other work, describing its
% main features and indicating the advances achieved. The abstract is
% meant to be succinct, not extending beyone about half a dozen lines,
% but should give potential readers an idea of the paper's potential
% importance.\\
Single-pass Point Rendering conventionally requires two geometry passes: a depth pass and a color pass to blend the visible fragments. The paper by Zhang and Pajarola outlines a method to combine these two passes into one, deferring some of the depth checking and all of the blending to an image compositing pass. For this, the set of splats to be rendered is split up into internally disjoint subsets which are rendered separately without blending, since their internal disjointness guarantees this is unnecessary. During deferred shading, depth testing and blending is performed. The results of Zhang and Pajarola (of 2004) show that the method is feasible only for very large models. We have implemented the algorithm and found that...}\\
\\
\\
\parbox{140truemm}{\normalsize
{\bfseries Keywords:} 
Computer Graphics, PBR, Point-based rendering, single pass}
} %end author

\date{}


\maketitle 

\section{Introduction} 
This is a
guide for authors on how to prepare papers for publication at the
EG-UK conference. It is itself prepared in the style to be adopted
using \LaTeX. The document is prepared for printing on A4
(UK) paper with 2.54 cm (one inch) margins at left, right, top and
bottom. There should be no page numbers, these will be added at the
time of publication. The fonts used, Times, Helvetica and Symbol, are
widely available and should pose no difficulties to authors. Authors
should be able to create a document by cutting and pasting into this
form, using the styles available within the document.  The indicative
addresses above are intended to be fictitious and the owners of this
guide accept no responsibility if the addresses actually exist.  


\section{Details of fonts and paragraphing} 
The following section contains
descriptions of the fonts and paragraph alignments to be used for
different parts of the document. The fonts used, Times, Helvetica and
Symbol, are widely available and should pose no difficulties to
authors.  

\subsection{The start section} 
The title is in 14 point Helvetica
bold, centred wtih 1.5 line spacing, a 48 point gap before the title,
12 point gap after it (style `Title').  The authors' names (style
`Author') are in centred 12 point Helvetica, single spaced following
directly below the title. If authors have different affiliations, an
asterisk${}^\ast$, dagger${}^\dag$ or other signifying marks may be used to
identify their contact details which appear below their
names. Otherwise, these should not appear. Different authors at the
same institution should combine email addresses as indicated above.
Addresses (style `Address') are similar to authors names, but in 12
point Times. A blank line should be left between the details for
several authors.  A brief abstract should be supplied below the word
`Abstract' in bold 10 point Times with a 48 point space above it
(style `Abstracthead'). This section is fully justified with 1 cm
indents at the right and left, the short text following without a line
gap below the title (style `Abstract').  A list of up to half a dozen
or so keywords with similar indents and font is spaced 12 points below
the abstract with a 24 point gap following. `Keywords:' should appear
in bold at the start of the list.  

\subsection{The main text} 
The main text is
in 10 point Times, fully justified in a two column format.  Main
headings, which should be sequentially numbered, are left justified in
12 point Helvetica bold with a 0.5 cm hanging indent (see, for
example, the heading for section 2, style `Heading1').  

\begin{figure}[!th]
\hrule
\begin{center}
\vspace*{2ex}
\framebox[2in]{\vspace{1in}Picture goes here \vspace{1in}}
\end{center}
\caption{An example of a line drawing illustrating Cartesian coordinates}
\label{fig:cart} 
\vspace*{2ex}
\hrule
\end{figure}

Sub
headings (numbered as 2.1, 2.2, ... 2.9, 2.10, etc.) are in 10 point
Helvetica italic with a 0.5 cm hanging indent (as in 2.1, style
`Heading2'). Both full and sub heading styles have a 6 point gap above
and below.  After headings, and if starting a paragraph after diagrams
and tables, the style `Startpara' is used. Subsequent paragraphs use
`Newpara', adding a 0.5 cm indent on the first line. There should be
no blank lines between paragraphs.  

\section{Figures and captions}

\begin{figure*}[!th]
\hrule
\begin{center}
\vspace*{2ex}
\framebox[4in]{\vspace{1in}Pictures go here \vspace{1in}}
\end{center}
\caption{An example of a figure extending across the width of a whole
page.  The picture shows some L-systems `plants' with random features}
\label{fig:full_width}
\vspace*{2ex}
\hrule
\end{figure*}

Figure~\ref{fig:cart} gives an example of a figure and its caption. 
In referring to
a figure, the full word `Figure' should be used if it appears at the
start of a sentence, as above, but a reference in the middle of a
sentence should take the form (fig.~\ref{fig:fungus}). 
When working in \LaTeX, figures
and their captions should appear in a frame as close as is reasonably
possible to the textual reference---however, this doesn't mean that
they will! You may want to consider placing
them at the tops or bottoms of columns---good luck.  Figures may
extend across the full width of the page (fig.~\ref{fig:full_width}) 
or across one column; attempting to achieve otherwise is (a) difficult
and (b) very time consuming. A better way is to shrink your image
to fit one column or centre the image in a page-wide
frame.  We have now referred to four figures in one paragraph. This is
an example of how it can be very difficult to have all figures visible
from the point of reference.  Figure captions are in 10 point
Helvetica, centred below the image (see, for example, 
figs.~\ref{fig:cart} and~\ref{fig:two_imgs}),
with reasonable spacing between the caption, the image and the main
text. Word can make it difficult to identify these styles in the
correct location, so it is probably easier to create these by choosing
the correct alignment, font and size directly.  Fully rendered images
may be included, but if they are created in colour, they should be of
a form suitable to reproduce in half-tone, as colour will not be used
in the conference proceedings. This could be achieved by adjusting
contrast and brightness using a proprietary paint package. 
Figure~\ref{fig:two_imgs}
indicates how colour image (a) can appear in half tone (b).

\begin{figure}[!th]
\hrule
\begin{center}
\vspace*{2ex}
\framebox[2in]{\vspace{1in}Pictures go here \vspace{1in}}
\end{center}
\caption{A diagram (the typical structure of a fungus  `mycelium') that
extends across columns}
\label{fig:fungus}
\vspace*{2ex}
\hrule
\end{figure}

The figures shown here have the illustration
boxed in with the captions below. This is not a hard and fast rule,
but authors should use a consistent method.  

\section{References} 
It is
expected that references should be made to books or chapters and
papers in journals or conference proceedings. Examples of these styles
are give in the references.
When referring to these from the text,
giving the full author's name or authors' names with the year of
publication is preferred \citep{sgbti}. If names are specificially
referred to in the text, they should appear out of the brackets with
the date in brackets afterwards, so \citet{abp}
set the standard for a number of researchers in plant growth
simulation. When there are more than two authors, only the first
should be directly named~\cite{cgpp}. If referring to specific
pages in a book or long paper, this may be done at the point of
reference, so we know that Gouraud shading is an approximation to
physical models of light~\cite[][p. 735]{cgpp}.

\begin{figure*}[!th]
\hrule
\begin{center}
\vspace*{2ex}
\framebox[4in]{\vspace{1in}Pictures go here \vspace{1in}}
\end{center}
\caption{An example of a colour image (left) and its half tone
equivalent with constrast and brightness adjusted. It shows a
simulation of an ocean wave  passing an obstruction near the surface.}
\label{fig:two_imgs}
\vspace*{2ex}
\hrule
\end{figure*}

In the reference list, there are different styles for presenting
\begin{itemize}
\item books, 
\item chapters, 
\item journal papers, 
\item conference proceedings papers, 
\item web publications. 
\end{itemize}
These all have a 0.5 cm hanging indent,
as illustrated in section 10. The list above is an examples of a
bullet list (see section~\ref{sec:list} below). 

\subsection{Books and chapters} 
In the
reference list, book references~\citep{cgpp} contain author(s),
date of publication (found from the copyright page), title in italic,
publisher, location of publication (the first place in the list on the
title page).  A book chapter contains the names of chapter authors,
year of publication, the chapter title, `in' the book title in italic,
editors, publisher, location of publication and chapter page numbers
\citep{anfifs}. 

\subsection{Papers in journals and conference proceedings} 
\citet{sgbti} is an example of a journal paper, with
author, date of publication, title of paper, name of journal in
italic, Volume number in bold and journal number if applicable, paper
page numbers.  Conference papers are similarly referenced, but this
time the organisers, location of the event and more precise date
information may be included \citep{ifsnat}. For tutorial
presentations, discretion can be used to present necessary identifying
information \citep{ifsrifs}.  

\subsection{Web publications}
Web publications could
be listed separately below the standard reference list, or could be
interspersed with the reference list, giving author, date of first
appearance, full URL.
 
\section{Equations}
Equations are indented 0.5 cm to left and right of the
column with a further hanging left indent of 0.5 cm if they extend
beyond the first line. Personal judgement should be used in deciding
where to split such equations. They are not justified and are preceded
and followed by 6 point spaces (style `Equation'), as in the
following.  
\begin{eqnarray*}
P = (1 - b)\{(1 - a)V_0 + aV_1\} + \\ 
\quad b\{(1 - a)V_3 + aV_2\}.
\end{eqnarray*}
Symbol
font should be used for Greek characters when needed, but it may be
worth reducing the font size to 9 point Symbol to fit in with Times 10
point.  
$$a_2 = b_2 + c_2 - 2bc \cos(\alpha)$$
The Word formula builder can be
useful, but authors who may not have access to this could create
equations as graphics objects to be cut and pasted into documents as
required.  

\section{Tables and lists}
\label{sec:list}
 Tables should have numerical values
right or decimal point justified to a precision appropriate to the
problem at hand. Cell margins should be indicated as required, but a
bounding margin should appear to all tables. Table captions should
take the same format as figure captions, but should appear above
figures rather than below.  Lists may be bulleted (style `Bulletlist')
or numbered in an equivalent format (`Numberedlist'), as shown in
section 7 below 

\section{Advice for new authors}
This section is intended as
advice for those not familiar with preparing papers for journal or
conference presentation, more experienced presenters will not need to
read this.  

\subsection{Paper structuring}

As a general guideline, the of the
paper should be coherently organised, typically along the following
lines.  

\begin{enumerate}
\item Introduction, general indication of the problem to be
discussed.  
\item Relevant work of others; this is where many of your
references should appear.  
\item Explanation, possibly in several
sections, of the work you have undertaken.  
\item Discussion of the
results achieved.  
\item Critical analysis of these results and an
indication of further work that could follow on from them.
\item Acknowledgements 
\item References 
\end{enumerate}

\subsection{Tips on presentation}
\begin{itemize}
\item Do speck chell.  
\item Make sure you know the meaning of `its' and `it's'.  
\item Avoid
colloquialisms and popular abbreviations, so if you are tempted to
write `it's', you are almost certainly wrong.  
\item Use the words
`affect' and `effect' appropriately. As a verb, `affect' means to
change the state of, `effect' means to put into action. As a noun,
`effect' is an observed change of some form.  
\item Beware of spaces and
punctuation. Do not have spaces before commas, full stops, closing
brackets, and other punctuation marks except for opening brackets,
which should not have spaces after them.  
\item Many authors write in the
third person, which has become the accepted norm for scientific
papers. This is not a hard and fast rule, but be consistent. Remember,
I told you so.  
\end{itemize}

\section{Summary} This is an indicative guide to authors for
publication of papers for EG-UK conference proceedings
publications. Accepted papers will appear in A4 format and must not
exceed 8 pages in length, including references and figures.  

\section{Acknowledgements}
 Members of the EG-UK executive committee have
collaborated in discussion of these guidelines.  

%% BEGIN INCLUDE
\bibliographystyle{eguk2000}
\bibliography{EGUKstyle}
\end{document}
%% END INCLUDE
