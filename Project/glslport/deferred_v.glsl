#version 150

in vec4 v;

out vec2 coord;

void main()
{
  gl_Position =  v;
  coord = (v.xy+1)/2.0;
}