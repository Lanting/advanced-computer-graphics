#version 150


// layout(location = 0) in vec4 pin;
// layout(location = 1) in vec4 colin;
// layout(location = 2) in vec3 uvec;
// layout(location = 3) in vec3 vvec;

in vec4 pin;
in vec4 colin;
in vec3 uvec;
in vec3 vvec;


uniform mat4  ModelViewProj;
uniform mat4  ModelView;
uniform vec2  wsize;
uniform float near;
uniform float top;
uniform float bottom;

// layout(location = 0) out vec4 pout;
// layout(location = 1) out vec4 colout;
// layout(location = 2) out vec3 v1;
// layout(location = 3) out vec3 v2;
// layout(location = 4) out vec3 v3;
// layout(location = 5) out float depth;
// layout(location = 6) out float radius;

out Data{
  vec4 p;
  vec4 c;
  vec3 v1;
  vec3 v2;
  vec3 v3;
  float depth;
  float radius;
} dataOut;

void main()
{
  dataOut.p = ModelViewProj * pin;
  vec4 peye = ModelView * pin;
  vec4 ueye = ModelView * vec4(uvec,0);
  vec4 veye = ModelView * vec4(vvec,0);

  dataOut.radius = sqrt(max(dot(uvec,uvec),dot(vvec,vvec)));

  gl_PointSize = 2.0 * dataOut.radius * (-near / peye.z) * (wsize.y / (top - bottom));

  dataOut.c = colin;

  dataOut.v1       = cross(peye.xyz, - veye.xyz);
  dataOut.v2       = cross(-ueye.xyz, peye.xyz);
  dataOut.v3       = cross(ueye.xyz, veye.xyz);
  dataOut.depth    = dot(peye.xyz, dataOut.v3);

  gl_Position = dataOut.p;
  //backface culling
  if (dataOut.depth > 0.0)
    gl_Position.w = -1.0;
}
