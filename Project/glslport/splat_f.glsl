#version 150

// layout(location = 0) in vec4 pin;
// layout(location = 1) in vec4 colin;
// layout(location = 2) in vec3 v1;
// layout(location = 3) in vec3 v2;
// layout(location = 4) in vec3 v3;
// layout(location = 5) in float depthin;
// layout(location = 6) in float radiusin;

in Data{
  vec4 p;
  vec4 c;
  vec3 v1;
  vec3 v2;
  vec3 v3;
  float depth;
  float radius;
} dataIn;

uniform vec2 unproj_scale;
uniform vec2 unproj_offset;
uniform float near;
uniform float zb_scale;
uniform float zb_offset;
uniform float epsilon;

// layout(location = 0) out vec4 colout;
// layout(location = 1) out vec4 normout;
out vec4 colout;
out vec4 normout;

void main()
{
  vec3 q = vec3(gl_FragCoord.xy*unproj_scale - unproj_offset, -near);
  float dn = dot(q, dataIn.v3); //q_n^t (u_i cross v_i)
  float u = dot(q, dataIn.v1)/dn;
  float v = dot(q, dataIn.v2)/dn;
  float dist_sq = u*u + v*v;
  if(dist_sq > 1.0)
    discard;
  float dist = sqrt(dist_sq);
  float qz = q.z * dataIn.depth / dn - epsilon * dataIn.radius; //lambda = depthin/dn
  gl_FragDepth = zb_scale/qz + zb_offset;
  //colorout = float4(col.xyz, col.w*(1-dist_sq));
  float weight = smoothstep(1,0,dist);
  colout = dataIn.c * weight;
  normout = vec4(normalize(dataIn.v3),1)*weight;
}