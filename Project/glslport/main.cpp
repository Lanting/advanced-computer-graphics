#include <assert.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <cmath>

#include <GL/glew.h>
#include <GL/glut.h>

#define GLM_FORCE_RADIANS
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

int w_width=512, w_height=512;
float near, top, bottom;
float epsilon = 1e0;

int lastTime = 0;
int nframes = 0;

struct Surfel
{
    GLfloat pos[3];
    GLfloat color[3]; // Changed from 4 to save space
    GLfloat uvec[3];
    GLfloat vvec[3];
};

int numpoints;
Surfel *pts;
GLuint ptsVbo;

static struct {
    GLuint program;
    GLuint ModelViewProj;
    GLuint ModelView;
    GLuint ModelViewIT;
    GLuint wsize;
    GLuint near;
    GLuint top;
    GLuint bottom;
    GLuint unproj_scale;
    GLuint unproj_offset;
    GLuint zb_scale;
    GLuint zb_offset;
    GLuint epsilon;
} splatShader;

static struct  {
    GLuint fbo;
    GLuint depthbuffer;
    GLuint colorbuffer;
    GLuint normalbuffer;
} splatbuffer = {0,0,0,0};

glm::mat4 splatModelMatrix;
glm::mat4 splatViewMatrix;
glm::mat4 splatProjMatrix;

static struct {
    GLuint program;
    GLuint colors;
    GLuint normals;
    GLuint unproj_scale;
    GLuint unproj_offset;
    GLuint near;
} deferred;

const GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};

void read_points(const char *fname)
{
    FILE *f = fopen(fname, "rb");
    assert(f);
    fread(&numpoints, sizeof(int), 1, f);
    printf("Reading %i points from %s ...\n", numpoints, fname);
    pts = new Surfel[numpoints];
    fread(pts, sizeof(Surfel)*numpoints, 1, f);    
    fclose(f);
    
    //fill vbo
    glGenBuffers(1, &ptsVbo);
    glBindBuffer(GL_ARRAY_BUFFER, ptsVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Surfel)*numpoints, pts, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key) {
        case 'q':
        case 'Q':
        case 27:
            exit(0);
            break;
    }
}

void display() 
{
    int curTime = glutGet(GLUT_ELAPSED_TIME);
    nframes += 1;
    if ((curTime - lastTime) > 10000){
        printf("fps: %5.3f\n", 1000.0 * nframes / (double) (curTime - lastTime));
        nframes = 0;
        lastTime = curTime;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 1e-8);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    GLdouble fW, fH;
    float aspect = (float)w_width/(float)w_height;
    near = 0.1;
    float far = 100.0;
    float fovY = 30.0;
    fH = tan( fovY / 360 * M_PI ) * near;
    fW = fH * aspect;
    top = fH;
    bottom = -fH;
    splatProjMatrix = glm::frustum( -fW, fW, -fH, fH, (double)near, (double)far);

    splatViewMatrix = glm::lookAt(
        glm::vec3(4, 0, -3),
        glm::vec3(0, 0, 0),
        glm::vec3(0, 1, 0));
    
    splatModelMatrix = glm::rotate(
        glm::mat4(1.0f),
        (float)curTime/20.0f * 0.0174532925f, //deg to rad
        glm::vec3(0.f, 1.f, 0.f));

    glm::mat4 ModelView = splatViewMatrix * splatModelMatrix;
    glm::mat4 ModelViewProj = splatProjMatrix * ModelView;

    glUseProgram(splatShader.program);

    //glsl uniforms
    glUniformMatrix4fv(
        splatShader.ModelViewProj,
        1,
        GL_FALSE,
        glm::value_ptr(ModelViewProj));
    glUniformMatrix4fv(
        splatShader.ModelView,
        1,
        GL_FALSE,
        glm::value_ptr(ModelView));

    glUniform2f( splatShader.wsize, w_width, w_height);
    glUniform1f(  splatShader.near, near);
    glUniform1f(   splatShader.top, top);
    glUniform1f(splatShader.bottom, bottom);
    glUniform2f(splatShader.unproj_scale, (2*fW)/w_width, (2*fH)/w_height);
    glUniform2f(splatShader.unproj_offset, fW, fH);
    glUniform1f(splatShader.zb_scale, far*near/(far-near));
    glUniform1f(splatShader.zb_offset, far/(far-near));
    glUniform1f(splatShader.epsilon, epsilon);
    
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
    
    //glBindBuffer(GL_ARRAY_BUFFER, 0);
    //char *base = (char *) pts;
    glBindBuffer(GL_ARRAY_BUFFER, ptsVbo);
    char *base = 0;
    
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Surfel), base + offsetof(Surfel, pos));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Surfel), base + offsetof(Surfel, color));
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Surfel), base + offsetof(Surfel, uvec));
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Surfel), base + offsetof(Surfel, vvec));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glEnableClientState(GL_VERTEX_ARRAY);

    //switch fbo
    glBindFramebuffer(GL_FRAMEBUFFER, splatbuffer.fbo);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //depth pass
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    glDepthMask(GL_TRUE);
    glDrawArrays(GL_POINTS, 0, numpoints);
    //color pass
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    //glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE, GL_ONE, GL_ONE);
    glUniform1f(splatShader.epsilon, 0);
    glDrawArrays(GL_POINTS, 0, numpoints);
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);

    glUseProgram(deferred.program);

    glUniform1i( deferred.colors, 0);
    glUniform1i( deferred.normals, 1);
    glUniform2f(deferred.unproj_scale, (2*fW)/w_width, (2*fH)/w_height);
    glUniform2f(deferred.unproj_offset, fW, fH);
    glUniform1f(  deferred.near, near);


    glActiveTexture(GL_TEXTURE0);
    glBindTexture (GL_TEXTURE_2D, splatbuffer.colorbuffer);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture (GL_TEXTURE_2D, splatbuffer.normalbuffer);
    GLfloat fsQuad[] = {
        -1.0f, -1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,
         1.0f, -1.0f, 0.0f,
         1.0f,  1.0f, 0.0f};
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, fsQuad);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glDisableClientState(GL_VERTEX_ARRAY);

    glUseProgram(0);

    //print any opengl errors
    int error;
    while ((error = glGetError()) != GL_NO_ERROR) {
        printf("GLerror: %s\n",  gluErrorString(error));
    }

    glutSwapBuffers();
}

GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path){

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
    if(VertexShaderStream.is_open()){
        std::string Line = "";
        while(getline(VertexShaderStream, Line))
            VertexShaderCode += "\n" + Line;
        VertexShaderStream.close();
    }else{
        printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
        getchar();
        return 0;
    }

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if(FragmentShaderStream.is_open()){
        std::string Line = "";
        while(getline(FragmentShaderStream, Line))
            FragmentShaderCode += "\n" + Line;
        FragmentShaderStream.close();
    }



    GLint Result = GL_FALSE;
    int InfoLogLength;



    // Compile Vertex Shader
    printf("Compiling shader : %s\n", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);

    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
        printf("%s\n", &VertexShaderErrorMessage[0]);
    }



    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);

    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
        printf("%s\n", &FragmentShaderErrorMessage[0]);
    }



    // Link the program
    printf("Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    return ProgramID;
}

void relink(GLuint ProgramID)
{
    glLinkProgram(ProgramID);

    GLint Result = GL_FALSE;
    int InfoLogLength;
    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }
}

void loadGLSLPrograms()
{
    splatShader.program = LoadShaders("splat_v.glsl", "splat_f.glsl");
    //set attributes the old skool way GAST UPDATE GODVERDOMME DIE LWP EENS
    glBindAttribLocation(splatShader.program, 0, "pin");
    glBindAttribLocation(splatShader.program, 1, "colin");
    glBindAttribLocation(splatShader.program, 2, "uvec");
    glBindAttribLocation(splatShader.program, 3, "vvec");
    glBindFragDataLocation(splatShader.program, 0, "colout");
    glBindFragDataLocation(splatShader.program, 1, "normout");
    //relink
    relink(splatShader.program);
    //uniforms
    splatShader.ModelViewProj = glGetUniformLocation(splatShader.program, "ModelViewProj");
    splatShader.ModelView     = glGetUniformLocation(splatShader.program, "ModelView");
    splatShader.wsize         = glGetUniformLocation(splatShader.program, "wsize");
    splatShader.near          = glGetUniformLocation(splatShader.program, "near");
    splatShader.top           = glGetUniformLocation(splatShader.program, "top");
    splatShader.bottom        = glGetUniformLocation(splatShader.program, "bottom");
    splatShader.unproj_scale  = glGetUniformLocation(splatShader.program, "unproj_scale");
    splatShader.unproj_offset = glGetUniformLocation(splatShader.program, "unproj_offset");
    splatShader.zb_scale      = glGetUniformLocation(splatShader.program, "zb_scale");
    splatShader.zb_offset     = glGetUniformLocation(splatShader.program, "zb_offset");
    splatShader.epsilon       = glGetUniformLocation(splatShader.program, "epsilon");

    deferred.program       = LoadShaders("deferred_v.glsl", "deferred_f.glsl");
    deferred.colors        = glGetUniformLocation(deferred.program, "colors");
    deferred.normals       = glGetUniformLocation(deferred.program, "normals");
    deferred.unproj_scale  = glGetUniformLocation(deferred.program, "unproj_scale");
    deferred.unproj_offset = glGetUniformLocation(deferred.program, "unproj_offset");
    deferred.near          = glGetUniformLocation(deferred.program, "near");
}

void idle()
{
    glutPostRedisplay();
}

void reshape(int width, int height)
{
    w_width = width;
    w_height = height;
    glViewport(0, 0, w_width, w_height);

    //fbo magic
    //delete the old buffers
    glDeleteFramebuffers(1, &(splatbuffer.fbo));
    glDeleteTextures(1, &(splatbuffer.depthbuffer));
    glDeleteTextures(1, &(splatbuffer.colorbuffer));
    glDeleteTextures(1, &(splatbuffer.normalbuffer));
    //init the new color buffer
    glGenTextures(1, &(splatbuffer.colorbuffer));
    glBindTexture(GL_TEXTURE_2D, splatbuffer.colorbuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D, //target
        0,             //mipmap level
        GL_RGBA16F,    //internalFormat
        w_width,       //width
        w_height,      //height
        0,             //border
        GL_RGBA,       //format
        GL_FLOAT,      //type
        0);            //data
    glBindTexture(GL_TEXTURE_2D, 0);
    //normal buffer
    glGenTextures(1, &(splatbuffer.normalbuffer));
    glBindTexture(GL_TEXTURE_2D, splatbuffer.normalbuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D, //target
        0,             //mipmap level
        GL_RGBA16F,    //internalFormat
        w_width,       //width
        w_height,      //height
        0,             //border
        GL_RGBA,       //format
        GL_FLOAT,      //type
        0);            //data
    glBindTexture(GL_TEXTURE_2D, 0);
    //depth buffer
    glGenTextures(1, &(splatbuffer.depthbuffer));
    glBindTexture(GL_TEXTURE_2D, splatbuffer.depthbuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D, //target
        0,             //mipmap level
        GL_DEPTH_COMPONENT32,//internalFormat
        w_width,       //width
        w_height,      //height
        0,             //border
        GL_DEPTH_COMPONENT,//format
        GL_FLOAT,      //type
        0);            //data
    glBindTexture(GL_TEXTURE_2D, 0);
    //frame buffer
    glGenFramebuffers(1, &(splatbuffer.fbo));
    glBindFramebuffer(GL_FRAMEBUFFER, splatbuffer.fbo);
    //attach color
    glFramebufferTexture2D(
          GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
          GL_COLOR_ATTACHMENT0,  // 2. attachment point
          GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
          splatbuffer.colorbuffer,// 4. tex ID
          0);                    // 5. mipmap level: 0(base)
    //attach normals
    glFramebufferTexture2D(
          GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
          GL_COLOR_ATTACHMENT1,  // 2. attachment point
          GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
          splatbuffer.normalbuffer,// 4. tex ID
          0);                    // 5. mipmap level: 0(base)
    //attach depth
    glFramebufferTexture2D(
          GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
          GL_DEPTH_ATTACHMENT,  // 2. attachment point
          GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
          splatbuffer.depthbuffer,// 4. tex ID
          0);                    // 5. mipmap level: 0(base)

    glDrawBuffers(2, buffers);

    int error;
    if ((error = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE)
        std::printf("paniek: framebuffer fail:  %s\n",  gluErrorString(error));

    // set the old fb back
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void initGlut(int argc, char *argv[]){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(w_width, w_height);
    glutCreateWindow("Point based rendering in GLSL");
    
    glewInit();
}

int main(int argc, char *argv[])
{
    initGlut(argc, argv);
    
    glutKeyboardFunc(keyboard);
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutReshapeFunc(reshape);
    
    loadGLSLPrograms();
    
    assert(argc>=2 && argv[1]);
    read_points(argv[1]);
    
    glutMainLoop();
    
    return 0;
}


