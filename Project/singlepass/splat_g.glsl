#version 150

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

const vec2 coords[4] = vec2[4](
    vec2(-1, -1),
    vec2( 1, -1), 
    vec2(-1,  1), 
    vec2( 1,  1));

in Data1{
  vec4 p;
  vec4 c;
  vec3 u;
  vec3 v;
} dataIn[];

out Data2{
  //vec4 p;
  vec4 c;
  vec2 uv;
  vec3 normal;
} dataOut;

uniform mat4  ModelViewProj;
uniform mat4  ModelView;
uniform float epsilon;
uniform float zb_scale;

void main()
{
  //dataOut.p = dataIn[0].p;
  vec4 ueye = ModelView*vec4(dataIn[0].u,0);
  vec4 veye = ModelView*vec4(dataIn[0].v,0);

  dataOut.normal = normalize(cross(ueye.xyz, veye.xyz));
  if (dataOut.normal.z > 0)
  {
    dataOut.c = dataIn[0].c;

    float radius = sqrt(max(dot(dataIn[0].u,dataIn[0].u),dot(dataIn[0].v,dataIn[0].v)));
    float z_offset = epsilon * radius;

    for (int i = 0; i < 4; i++)
    {
      dataOut.uv = coords[i];
      vec4 p = dataIn[0].p;
      p.xyz +=  
          + dataOut.uv.x * dataIn[0].u
          + dataOut.uv.y * dataIn[0].v;
      gl_Position = ModelViewProj * p;
      gl_Position.z += z_offset;
      EmitVertex();
    }
  }
}