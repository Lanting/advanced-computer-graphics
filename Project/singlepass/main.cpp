#include <assert.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <set>

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <cmath>

#include <GL/glew.h>
#include <GL/glut.h>

#define GLM_FORCE_RADIANS
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include "nanoflann.hpp"


// Variables and datastructures
int w_width=512, w_height=512;
float near, top, bottom;

int lastTime = 0;
int nframes = 0;

struct Surfel
{
    GLfloat pos[3];
    GLfloat color[3]; // Changed from 4 to save space
    GLfloat uvec[3];
    GLfloat vvec[3];
};

GLfloat max_radius;

int numpoints;
Surfel *pts;
GLuint ptsVbo;

std::vector<std::vector<GLuint>> surf_subsets;
GLuint subsetVbo;
std::vector<GLuint> subset_offsets;
int numSubsets;
GLfloat overlap_relaxation = 1;

static struct {
    GLuint program;
    GLuint ModelViewProj;
    GLuint ModelView;
    GLuint ModelViewIT;
    GLuint wsize;
    GLuint near;
    GLuint top;
    GLuint bottom;
    GLuint unproj_scale;
    GLuint unproj_offset;
    GLuint zb_scale;
    GLuint zb_offset;
    GLuint epsilon;
} splatShader;

struct DeferredBuffer {
    GLuint fbo;
    GLuint depthbuffer;
    GLuint colorbuffer;
    GLuint normalbuffer;
    DeferredBuffer()
    : fbo(0), depthbuffer(0), colorbuffer(0), normalbuffer(0)
    {};
} *splatbuffers, splatbuffer, blendbuffer;

glm::mat4 splatModelMatrix;
glm::mat4 splatViewMatrix;
glm::mat4 splatProjMatrix;

static struct {
    GLuint program;
    GLuint colors;
    GLuint normals;
    GLuint unproj_scale;
    GLuint unproj_offset;
    GLuint near;
} deferred;

static struct {
    GLuint program;
    GLuint colors;
    GLuint normals;
    GLuint epsilon;
} blend;

const GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};

int maxTextureImageUnits;

//pointcloud class for use with flann
// http://nanoflann.googlecode.com/svn/trunk/examples/pointcloud_kdd_radius.cpp
struct PointCloud
{
    struct Point
    {
        GLfloat pos[3];
        GLfloat color[3]; // Changed from 4 to save space
        GLfloat uvec[3];
        GLfloat vvec[3];
        GLfloat radius;
        std::list<size_t> neighbors;
        int group;
    };
    std::vector<Point> pts;

    GLfloat max_radius;

    // Must return the number of data points
    inline size_t kdtree_get_point_count() const { return pts.size(); }

    // Returns the distance between the vector "p1[0:size-1]" and the data point with index "idx_p2" stored in the class:
    inline GLfloat kdtree_distance(const GLfloat *p1, const size_t idx_p2,size_t size) const
    {
        const GLfloat d0=p1[0]-pts[idx_p2].pos[0];
        const GLfloat d1=p1[1]-pts[idx_p2].pos[1];
        const GLfloat d2=p1[2]-pts[idx_p2].pos[2];
        return d0*d0+d1*d1+d2*d2;
    };

    // Returns the dim'th component of the idx'th point in the class:
    // Since this is inlined and the "dim" argument is typically an immediate value, the
    //  "if/else's" are actually solved at compile time.
    inline GLfloat kdtree_get_pt(const size_t idx, int dim) const
    {
        if (dim==0) return pts[idx].pos[0];
        else if (dim==1) return pts[idx].pos[1];
        else return pts[idx].pos[2];
    }

    // Optional bounding-box computation: return false to default to a standard bbox computation loop.
    //   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
    //   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
    template <class BBOX>
    bool kdtree_get_bbox(BBOX &bb) const { return false; }

    PointCloud(Surfel *sfls, size_t numpoints)
    :
        max_radius(0)
    {
        pts.resize(numpoints);
        for (size_t idx = 0; idx < numpoints; ++idx){
            memcpy(&(pts[idx]), sfls + idx, sizeof(Surfel));
            GLfloat ul2 =
                    pts[idx].uvec[0]*pts[idx].uvec[0] +
                    pts[idx].uvec[1]*pts[idx].uvec[1] +
                    pts[idx].uvec[2]*pts[idx].uvec[2];

            GLfloat vl2 =
                    pts[idx].vvec[0]*pts[idx].vvec[0] +
                    pts[idx].vvec[1]*pts[idx].vvec[1] +
                    pts[idx].vvec[2]*pts[idx].vvec[2];
            pts[idx].radius = overlap_relaxation*sqrt(std::max(ul2, vl2));
            pts[idx].group = -1;
            max_radius = std::max(max_radius, pts[idx].radius);
        }
    }
};

// splat processing funtions
void read_points(const char *fname)
{
    FILE *f = fopen(fname, "rb");
    assert(f);
    fread(&numpoints, sizeof(int), 1, f);
    printf("Reading %i points from %s ...\n", numpoints, fname);
    pts = new Surfel[numpoints];
    fread(pts, sizeof(Surfel)*numpoints, 1, f);
    fclose(f);
}

void process_points()
{
    //generate pointcloud for nanoflann to work with
    printf("building tree..."); fflush(stdout);
    PointCloud cloud(pts, numpoints);
    max_radius = cloud.max_radius;

    // construct a kd-tree index:
    typedef nanoflann::KDTreeSingleIndexAdaptor<
        nanoflann::L2_Simple_Adaptor<GLfloat, PointCloud>,
        PointCloud,
        3 /* dim */
        > my_kd_tree_t;

    my_kd_tree_t   index(
        3 /*dim*/,
        cloud,
        nanoflann::KDTreeSingleIndexAdaptorParams(10 /* max leaf */));
    index.buildIndex();
    printf("done\n");
    printf("max radius 2*%f\n",cloud.max_radius);

    //build graph
    printf("building graph..."); fflush(stdout);
    nanoflann::SearchParams params;
    params.sorted=false;
    GLfloat search_radius = 4*cloud.max_radius*cloud.max_radius;
    #pragma omp parallel
    {
        std::vector<std::pair<size_t,GLfloat> >   ret_matches;
        #pragma omp for schedule(dynamic,1000)
        for (size_t idx = 0; idx < (size_t)numpoints; ++idx)
        {
            /*const size_t nMatches = */index.radiusSearch(
                cloud.pts[idx].pos,
                search_radius,
                ret_matches,
                params);
            // printf("idx: %9d, [%7.4f, %7.4f, %7.4f], matches: %4d\n",
            //     (int)idx,
            //     cloud.pts[idx].pos[0], cloud.pts[idx].pos[1], cloud.pts[idx].pos[2],
            //     (int)nMatches);
            for (auto it = ret_matches.begin(); it != ret_matches.end(); ++it)
            {   
                GLfloat r = cloud.pts[idx].radius + cloud.pts[it->first].radius;

                if (it->second < r*r)
                {
                    cloud.pts[idx].neighbors.push_back(it->first);
                }
            }
            // printf("actual overlaping: %d\n",cloud.pts[idx].neighbors.size());
        }
    }
    printf("done\n");

    //largest first requires the node with the largest degree to be handled first
    //easiest is to build a lookup table and sort that (as bookkeeping with the new edge definitions would be too combersome)
    std::vector<std::pair<int,int>> lookup; // pair(degree, index)
    lookup.reserve(cloud.pts.size());
    for (size_t idx = 0; idx != cloud.pts.size(); ++idx)
        lookup.emplace_back(cloud.pts[idx].neighbors.size() ,idx);
    std::sort(lookup.rbegin(), lookup.rend()); //descending order
    printf("max. degree: %d\n", lookup[0].first);


    //build subsets
    printf("determining subsets..."); fflush(stdout);
    for (auto lookup_it = lookup.begin(); lookup_it != lookup.end(); ++lookup_it)
    {
        auto surf_it = cloud.pts.begin() + lookup_it->second;
        std::set<int> takengroups;
        for (auto neighbor_it = surf_it->neighbors.begin(); neighbor_it != surf_it->neighbors.end(); ++neighbor_it)
        {
            takengroups.insert(cloud.pts[*neighbor_it].group);
        }
        //check which one is avaiable
        int group = -1;
        std::set<int>::iterator group_it;
        do {
            ++group;
            group_it = takengroups.find(group);
        } while (group_it != takengroups.end());
        surf_it->group=group;
        //copy surfel to that group
        if ((int)surf_subsets.size() <= group)
            surf_subsets.resize(group+1);
        //surf_subsets[group].push_back(*((Surfel*) &(*surf_it))); //pointer casting awesomeness
        surf_subsets[group].push_back(lookup_it->second); //just store the index
    }
    printf("done\n");
    numSubsets = surf_subsets.size();
    printf("Generated %i subsets\n", numSubsets);
    for (int i = 0; i < (int) surf_subsets.size(); ++i)
        printf("set: %2i, elements: %6i\n", (int) i, (int) surf_subsets[i].size());
}

void build_vbos()
{
    //fill vbo
    glGenBuffers(1, &ptsVbo);
    glBindBuffer(GL_ARRAY_BUFFER, ptsVbo);
    //glBufferData(GL_ARRAY_BUFFER, sizeof(Surfel)*surf_subsets[0].size(), surf_subsets[0][0].pos, GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Surfel)*numpoints, pts, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //build the element buffer
    std::vector<GLuint> subsets_conc;
    for (auto subset_it = surf_subsets.begin(); subset_it != surf_subsets.end(); ++subset_it)
    {
        subset_offsets.push_back(subsets_conc.size());
        subsets_conc.insert(subsets_conc.end(), subset_it->begin(), subset_it->end());
    }
    subset_offsets.push_back(subsets_conc.size());
    
    glGenBuffers(1, &subsetVbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, subsetVbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*numpoints, &subsets_conc[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    //build array of splatbuffers
    splatbuffers = new DeferredBuffer[numSubsets];
}

// visualisation
void keyboard(unsigned char key, int x, int y)
{
    switch(key) {
        case 'q':
        case 'Q':
        case 27:
            exit(0);
            break;
    }
}

void display() 
{
    int curTime = glutGet(GLUT_ELAPSED_TIME);
    nframes += 1;
    if ((curTime - lastTime) > 10000){
        printf("fps: %5.3f\n", 1000.0 * nframes / (double) (curTime - lastTime));
        nframes = 0;
        lastTime = curTime;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 1e-8);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    GLdouble fW, fH;
    float aspect = (float)w_width/(float)w_height;
    near = 0.1;
    float far = 10.0;
    float fovY = 30.0;
    fH = tan( fovY / 360 * M_PI ) * near;
    fW = fH * aspect;
    top = fH;
    bottom = -fH;
    splatProjMatrix = glm::frustum( -fW, fW, -fH, fH, (double)near, (double)far);

    splatViewMatrix = glm::lookAt(
        glm::vec3(4, 0, -3),
        glm::vec3(0, 0, 0),
        glm::vec3(0, 1, 0));
    
    splatModelMatrix = glm::rotate(
        glm::mat4(1.0f),
        (float)curTime/20.0f * 0.0174532925f, //deg to rad
        glm::vec3(0.f, 1.f, 0.f));

    glm::mat4 ModelView = splatViewMatrix * splatModelMatrix;
    glm::mat4 ModelViewProj = splatProjMatrix * ModelView;

    glUseProgram(splatShader.program);
    //glEnable(GL_CULL_FACE);

    //glsl uniforms
    glUniformMatrix4fv(
        splatShader.ModelViewProj,
        1,
        GL_FALSE,
        glm::value_ptr(ModelViewProj));
    glUniformMatrix4fv(
        splatShader.ModelView,
        1,
        GL_FALSE,
        glm::value_ptr(ModelView));

    glUniform2f( splatShader.wsize, w_width, w_height);
    glUniform1f(  splatShader.near, near);
    glUniform1f(   splatShader.top, top);
    glUniform1f(splatShader.bottom, bottom);
    glUniform2f(splatShader.unproj_scale, (2*fW)/w_width, (2*fH)/w_height);
    glUniform2f(splatShader.unproj_offset, fW, fH);
    glUniform1f(splatShader.zb_scale, far*near/(far-near));
    glUniform1f(splatShader.zb_offset, far/(far-near));
    // glUniform1f(splatShader.epsilon, epsilon);
    
    //glBindBuffer(GL_ARRAY_BUFFER, 0);
    //char *base = (char *) pts;
    glBindBuffer(GL_ARRAY_BUFFER, ptsVbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, subsetVbo);
    char *base = 0;

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Surfel), base + offsetof(Surfel, pos));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Surfel), base + offsetof(Surfel, color));
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Surfel), base + offsetof(Surfel, uvec));
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Surfel), base + offsetof(Surfel, vvec));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glEnableClientState(GL_VERTEX_ARRAY);


    for (int subsetNr = 0; subsetNr != numSubsets; ++subsetNr)
    {
        //switch fbo
        glBindFramebuffer(GL_FRAMEBUFFER, splatbuffers[subsetNr].fbo);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //geometry stage
        //glDrawArrays(GL_POINTS, 0, numpoints);
        glDrawElements(
            GL_POINTS, 
            subset_offsets[subsetNr+1] - subset_offsets[subsetNr], 
            GL_UNSIGNED_INT, 
            (void*)(sizeof(GLuint)*subset_offsets[subsetNr]));
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisable(GL_CULL_FACE);

    GLfloat fsQuad[] = {
        -1.0f, -1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,
         1.0f, -1.0f, 0.0f,
         1.0f,  1.0f, 0.0f};

    //blend stage
    glUseProgram(blend.program);
    glUniform1i( blend.colors, 0);
    glUniform1i( blend.normals, 1);

    glBindFramebuffer(GL_FRAMEBUFFER, blendbuffer.fbo);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnableClientState(GL_VERTEX_ARRAY);
    //blend depth pass
    glUniform1f( blend.epsilon, 0.01/(far-near)*max_radius);
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    glDepthMask(GL_TRUE);
    for (int subsetNr = 0; subsetNr != numSubsets; ++subsetNr)
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture (GL_TEXTURE_2D, splatbuffers[subsetNr].colorbuffer);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture (GL_TEXTURE_2D, splatbuffers[subsetNr].normalbuffer);

        glVertexPointer(3, GL_FLOAT, 0, fsQuad);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
    //blend 'blend' pass
    glUniform1f( blend.epsilon, 0.0);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    for (int subsetNr = 0; subsetNr != numSubsets; ++subsetNr)
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture (GL_TEXTURE_2D, splatbuffers[subsetNr].colorbuffer);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture (GL_TEXTURE_2D, splatbuffers[subsetNr].normalbuffer);

        glVertexPointer(3, GL_FLOAT, 0, fsQuad);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);

    glDisableClientState(GL_VERTEX_ARRAY);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glUseProgram(deferred.program);

    glUniform1i( deferred.colors, 0);
    glUniform1i( deferred.normals, 1);
    glUniform2f(deferred.unproj_scale, (2*fW)/w_width, (2*fH)/w_height);
    glUniform2f(deferred.unproj_offset, fW, fH);
    glUniform1f(  deferred.near, near);


    glActiveTexture(GL_TEXTURE0);
    glBindTexture (GL_TEXTURE_2D, blendbuffer.colorbuffer);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture (GL_TEXTURE_2D, blendbuffer.normalbuffer);

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, fsQuad);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glDisableClientState(GL_VERTEX_ARRAY);

    glUseProgram(0);

    //print any opengl errors
    int error;
    while ((error = glGetError()) != GL_NO_ERROR) {
        printf("GLerror: %s\n",  gluErrorString(error));
    }

    glutSwapBuffers();
}

GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path, const char * geom_file_path = NULL){

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
    GLuint GeomShaderID = glCreateShader(GL_GEOMETRY_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
    if(VertexShaderStream.is_open()){
        std::string Line = "";
        while(getline(VertexShaderStream, Line))
            VertexShaderCode += "\n" + Line;
        VertexShaderStream.close();
    }else{
        printf("Impossible to open %s. Are you in the right directory? Don't forget to read the FAQ!\n", vertex_file_path);
        getchar();
        return 0;
    }

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if(FragmentShaderStream.is_open()){
        std::string Line = "";
        while(getline(FragmentShaderStream, Line))
            FragmentShaderCode += "\n" + Line;
        FragmentShaderStream.close();
    }

    // Read the Fragment Shader code from the file
    std::string GeomShaderCode;
    if (geom_file_path)
    {
        std::ifstream GeomShaderStream(geom_file_path, std::ios::in);
        if(GeomShaderStream.is_open()){
            std::string Line = "";
            while(getline(GeomShaderStream, Line))
                GeomShaderCode += "\n" + Line;
            GeomShaderStream.close();
        }
    }



    GLint Result = GL_FALSE;
    int InfoLogLength;



    // Compile Vertex Shader
    printf("Compiling shader : %s\n", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);

    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
        printf("%s\n", &VertexShaderErrorMessage[0]);
    }



    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);

    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
        printf("%s\n", &FragmentShaderErrorMessage[0]);
    }

    // Compile Geometry Shader
    if (geom_file_path)
    {
        printf("Compiling shader : %s\n", geom_file_path);
        char const * GeomSourcePointer = GeomShaderCode.c_str();
        glShaderSource(GeomShaderID, 1, &GeomSourcePointer , NULL);
        glCompileShader(GeomShaderID);

        // Check Geom Shader
        glGetShaderiv(GeomShaderID, GL_COMPILE_STATUS, &Result);
        glGetShaderiv(GeomShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if ( InfoLogLength > 0 ){
            std::vector<char> GeomShaderErrorMessage(InfoLogLength+1);
            glGetShaderInfoLog(GeomShaderID, InfoLogLength, NULL, &GeomShaderErrorMessage[0]);
            printf("%s\n", &GeomShaderErrorMessage[0]);
        }
    }



    // Link the program
    printf("Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    if (geom_file_path)
        glAttachShader(ProgramID, GeomShaderID);
    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    return ProgramID;
}

void relink(GLuint ProgramID)
{
    glLinkProgram(ProgramID);

    GLint Result = GL_FALSE;
    int InfoLogLength;
    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }
}

void loadGLSLPrograms()
{
    splatShader.program = LoadShaders("splat_v.glsl", "splat_f.glsl", "splat_g.glsl");
    //set attributes the old skool way
    glBindAttribLocation(splatShader.program, 0, "pin");
    glBindAttribLocation(splatShader.program, 1, "colin");
    glBindAttribLocation(splatShader.program, 2, "uvec");
    glBindAttribLocation(splatShader.program, 3, "vvec");
    glBindFragDataLocation(splatShader.program, 0, "colout");
    glBindFragDataLocation(splatShader.program, 1, "normout");
    //relink
    relink(splatShader.program);
    //uniforms
    splatShader.ModelViewProj = glGetUniformLocation(splatShader.program, "ModelViewProj");
    splatShader.ModelView     = glGetUniformLocation(splatShader.program, "ModelView");
    splatShader.wsize         = glGetUniformLocation(splatShader.program, "wsize");
    splatShader.near          = glGetUniformLocation(splatShader.program, "near");
    splatShader.top           = glGetUniformLocation(splatShader.program, "top");
    splatShader.bottom        = glGetUniformLocation(splatShader.program, "bottom");
    splatShader.unproj_scale  = glGetUniformLocation(splatShader.program, "unproj_scale");
    splatShader.unproj_offset = glGetUniformLocation(splatShader.program, "unproj_offset");
    splatShader.zb_scale      = glGetUniformLocation(splatShader.program, "zb_scale");
    splatShader.zb_offset     = glGetUniformLocation(splatShader.program, "zb_offset");
    splatShader.epsilon       = glGetUniformLocation(splatShader.program, "epsilon");

    deferred.program       = LoadShaders("deferred_v.glsl", "deferred_f.glsl");
    deferred.colors        = glGetUniformLocation(deferred.program, "colors");
    deferred.normals       = glGetUniformLocation(deferred.program, "normals");
    deferred.unproj_scale  = glGetUniformLocation(deferred.program, "unproj_scale");
    deferred.unproj_offset = glGetUniformLocation(deferred.program, "unproj_offset");
    deferred.near          = glGetUniformLocation(deferred.program, "near");

    blend.program    = LoadShaders("blend_v.glsl", "blend_f.glsl");
    glBindFragDataLocation(blend.program, 0, "colout");
    glBindFragDataLocation(blend.program, 1, "normout");
    relink(blend.program);
    blend.colors     = glGetUniformLocation(blend.program, "colors");
    blend.normals    = glGetUniformLocation(blend.program, "normals");
    blend.epsilon    = glGetUniformLocation(blend.program, "epsilon");
}

void idle()
{
    glutPostRedisplay();
}

void reshape(int width, int height)
{
    w_width = width;
    w_height = height;
    glViewport(0, 0, w_width, w_height);

    //fbo magic GEOMETRY RENDERING PASS
    for (int buff_i = 0; buff_i != numSubsets; ++buff_i)
    {
        //delete the old buffers
        glDeleteFramebuffers(1, &(splatbuffers[buff_i].fbo));
        glDeleteTextures(1, &(splatbuffers[buff_i].depthbuffer));
        glDeleteTextures(1, &(splatbuffers[buff_i].colorbuffer));
        glDeleteTextures(1, &(splatbuffers[buff_i].normalbuffer));
        //init the new color buffer
        glGenTextures(1, &(splatbuffers[buff_i].colorbuffer));
        glBindTexture(GL_TEXTURE_2D, splatbuffers[buff_i].colorbuffer);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexImage2D(
            GL_TEXTURE_2D, //target
            0,             //mipmap level
            GL_RGBA16F,    //internalFormat
            w_width,       //width
            w_height,      //height
            0,             //border
            GL_RGBA,       //format
            GL_FLOAT,      //type
            0);            //data
        glBindTexture(GL_TEXTURE_2D, 0);
        //normal buffer
        glGenTextures(1, &(splatbuffers[buff_i].normalbuffer));
        glBindTexture(GL_TEXTURE_2D, splatbuffers[buff_i].normalbuffer);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexImage2D(
            GL_TEXTURE_2D, //target
            0,             //mipmap level
            GL_RGBA16F,    //internalFormat
            w_width,       //width
            w_height,      //height
            0,             //border
            GL_RGBA,       //format
            GL_FLOAT,      //type
            0);            //data
        glBindTexture(GL_TEXTURE_2D, 0);
        //depth buffer
        glGenTextures(1, &(splatbuffers[buff_i].depthbuffer));
        glBindTexture(GL_TEXTURE_2D, splatbuffers[buff_i].depthbuffer);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexImage2D(
            GL_TEXTURE_2D, //target
            0,             //mipmap level
            GL_DEPTH_COMPONENT32,//internalFormat
            w_width,       //width
            w_height,      //height
            0,             //border
            GL_DEPTH_COMPONENT,//format
            GL_FLOAT,      //type
            0);            //data
        glBindTexture(GL_TEXTURE_2D, 0);
        //frame buffer
        glGenFramebuffers(1, &(splatbuffers[buff_i].fbo));
        glBindFramebuffer(GL_FRAMEBUFFER, splatbuffers[buff_i].fbo);
        //attach color
        glFramebufferTexture2D(
              GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
              GL_COLOR_ATTACHMENT0,  // 2. attachment point
              GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
              splatbuffers[buff_i].colorbuffer,// 4. tex ID
              0);                    // 5. mipmap level: 0(base)
        //attach normals
        glFramebufferTexture2D(
              GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
              GL_COLOR_ATTACHMENT1,  // 2. attachment point
              GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
              splatbuffers[buff_i].normalbuffer,// 4. tex ID
              0);                    // 5. mipmap level: 0(base)
        //attach depth
        glFramebufferTexture2D(
              GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
              GL_DEPTH_ATTACHMENT,  // 2. attachment point
              GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
              splatbuffers[buff_i].depthbuffer,// 4. tex ID
              0);                    // 5. mipmap level: 0(base)

        glDrawBuffers(2, buffers);
    }

    //fbo magic BLENDING PASS
    //delete the old buffers
    glDeleteFramebuffers(1, &(blendbuffer.fbo));
    glDeleteTextures(1, &(blendbuffer.depthbuffer));
    glDeleteTextures(1, &(blendbuffer.colorbuffer));
    glDeleteTextures(1, &(blendbuffer.normalbuffer));
    //init the new color buffer
    glGenTextures(1, &(blendbuffer.colorbuffer));
    glBindTexture(GL_TEXTURE_2D, blendbuffer.colorbuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D, //target
        0,             //mipmap level
        GL_RGBA16F,    //internalFormat
        w_width,       //width
        w_height,      //height
        0,             //border
        GL_RGBA,       //format
        GL_FLOAT,      //type
        0);            //data
    glBindTexture(GL_TEXTURE_2D, 0);
    //normal buffer
    glGenTextures(1, &(blendbuffer.normalbuffer));
    glBindTexture(GL_TEXTURE_2D, blendbuffer.normalbuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D, //target
        0,             //mipmap level
        GL_RGBA16F,    //internalFormat
        w_width,       //width
        w_height,      //height
        0,             //border
        GL_RGBA,       //format
        GL_FLOAT,      //type
        0);            //data
    glBindTexture(GL_TEXTURE_2D, 0);
    //depth buffer
    glGenTextures(1, &(blendbuffer.depthbuffer));
    glBindTexture(GL_TEXTURE_2D, blendbuffer.depthbuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D, //target
        0,             //mipmap level
        GL_DEPTH_COMPONENT32,//internalFormat
        w_width,       //width
        w_height,      //height
        0,             //border
        GL_DEPTH_COMPONENT,//format
        GL_FLOAT,      //type
        0);            //data
    glBindTexture(GL_TEXTURE_2D, 0);
    //frame buffer
    glGenFramebuffers(1, &(blendbuffer.fbo));
    glBindFramebuffer(GL_FRAMEBUFFER, blendbuffer.fbo);
    //attach color
    glFramebufferTexture2D(
          GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
          GL_COLOR_ATTACHMENT0,  // 2. attachment point
          GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
          blendbuffer.colorbuffer,// 4. tex ID
          0);                    // 5. mipmap level: 0(base)
    //attach normals
    glFramebufferTexture2D(
          GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
          GL_COLOR_ATTACHMENT1,  // 2. attachment point
          GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
          blendbuffer.normalbuffer,// 4. tex ID
          0);                    // 5. mipmap level: 0(base)
    //attach depth
    glFramebufferTexture2D(
          GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
          GL_DEPTH_ATTACHMENT,  // 2. attachment point
          GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
          blendbuffer.depthbuffer,// 4. tex ID
          0);                    // 5. mipmap level: 0(base)

    glDrawBuffers(2, buffers);

    int error;
    if ((error = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE)
        std::printf("paniek: framebuffer fail:  %s\n",  gluErrorString(error));

    // set the old fb back
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void initGlut(int argc, char *argv[]){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(w_width, w_height);
    glutCreateWindow("Point based rendering in GLSL");
    
    glewInit();

    glutKeyboardFunc(keyboard);
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutReshapeFunc(reshape);
}

int main(int argc, char *argv[])
{
    assert(argc>=2 && argv[1]);
    if (argc >= 3)
        overlap_relaxation = std::stof(argv[2]);
    printf("overlap relaxation parameter: %f\n", overlap_relaxation);
    read_points(argv[1]);
    process_points();

    initGlut(argc, argv);
    
    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &maxTextureImageUnits);
    printf("GL_MAX_TEXTURE_IMAGE_UNITS: %3d\n", maxTextureImageUnits);
    loadGLSLPrograms();

    build_vbos();

    
    glutMainLoop();
    
    return 0;
}


