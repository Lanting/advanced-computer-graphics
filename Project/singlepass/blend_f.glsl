#version 150

in vec2 coord;

uniform sampler2D colors;
uniform sampler2D normals;

uniform float epsilon;

out vec4 colout;
out vec4 normout;

void main()
{
  normout = texture( normals, coord);
  colout = texture( colors, coord);

  gl_FragDepth = 1 - normout.w + epsilon;
}