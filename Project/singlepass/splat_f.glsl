#version 150

in Data2{
  //vec4 p;
  vec4 c;
  vec2 uv;
  vec3 normal;
} dataIn;

out vec4 colout;
out vec4 normout;

void main()
{
  float dist = length(dataIn.uv);
  if(dist > 1.0)
    discard;
  float weight = smoothstep(1,0,dist);
  colout = vec4(dataIn.c.rgb * weight, weight);
  //1-depth as we clear to 0 
  normout = vec4(dataIn.normal*weight, 1-gl_FragCoord.z);
}