#version 150

const float ka = 0.2;
const float kd = 0.8;
const float ks = 0.2;
const float shininess = 32.0;

const vec3 lightdir = normalize(vec3(0.5, 0.5, 1));
const vec4 lightcolor = vec4(1.0, 1.0, 1.0, 1.0);

in vec2 coord;

uniform vec2 unproj_scale;
uniform vec2 unproj_offset;
uniform float near;
uniform sampler2D colors;
uniform sampler2D normals;

out vec4 colorout;

void main()
{
  vec3 viewDir = normalize(-vec3(gl_FragCoord.xy*unproj_scale - unproj_offset, -near));

  colorout = texture( colors, coord);
  vec3 col = colorout.rgb / colorout.a;
  vec3 normal = texture(normals, coord).xyz / colorout.a;

  float diffuse = max(dot(normal, lightdir), 0.0);
  vec3 halfvec = normalize(lightdir + viewDir);
  float specular = pow(max(dot(normal, halfvec), 0.0), shininess);
  vec3 rgb = lightcolor.xyz * (ks*specular + col*(ka + kd*diffuse));


  colorout = vec4(rgb,1);
}