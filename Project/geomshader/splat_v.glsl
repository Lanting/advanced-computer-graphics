#version 150

in vec4 pin;
in vec4 colin;
in vec3 uvec;
in vec3 vvec;

out Data1{
  vec4 p;
  vec4 c;
  vec3 u;
  vec3 v;
} dataOut;

void main()
{
  dataOut.c = colin;
  dataOut.p = pin;
  dataOut.u = uvec;
  dataOut.v = vvec;
}
